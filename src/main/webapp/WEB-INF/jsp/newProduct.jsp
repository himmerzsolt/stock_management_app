<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
<!-- Access the bootstrap Css like this,
		Spring boot will handle the resource mapping automcatically -->
	<link rel="stylesheet" type="text/css" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css" />

	<!--
	<spring:url value="/css/main.css" var="springCss" />
	<link href="${springCss}" rel="stylesheet" />
	 -->
	<c:url value="/css/main.css" var="jstlCss" />
	<link href="${jstlCss}" rel="stylesheet" />
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>

<%@include file="header.jsp" %>


    	<form enctype="application/json" action="http://localhost:8080/shop/addNewItem" method="post">
             <input type="hidden"
            name="${_csrf.parameterName}"
            value="${_csrf.token}"/>

            <h5>To create a new product please fulfill the above fields ( Please note that all fields marked with an asterisk (*) are required.):</h5><br>
            <table id="create">
            <tr><td>Name of the wine:*</td><td><input type="text" name="name"></td>
            <td>Price:*</td><td><input type="number" name="price"></td></tr>
            <tr><td>Description:</td><td><input type="text" name="description"></td>
            <td>Type:*</td><td>
                                 <select name="type" size=”6”>
                                    <option value="WHITE" selected>WHITE</option>
                                    <option value="RED">RED</option>
                                    <option value="CHAMPAGNE">CHAMPAGNE</option>
                                    <option value="COGNAC">COGNAC</option>
                                    <option value="FRIZZANTE">FRIZZANTE</option>
                                    <option value="SPARKLING">SPARKLING</option>
                                 </select></td></tr>
            <tr><td>Quantity:</td><td><input type="number" name="quantity"></td>
            <td>Country:*</td><td><input type="text" name="country"></td></tr>
            <tr><td>Wine Region:*</td><td><input type="text" name="wineRegion"></td>
            <td>Winery:*</td><td><input type="text" name="winery"></td></tr>
            <tr><td>Vintage:*</td><td><input type="number" name="vintage"></td>
            <td>Content of sugar:*</td><td>
                 <select name="sugarContent" size=”10”>
                 <option value="DRY" selected>DRY</option>
                 <option value="SEMI_DRY">SEMI DRY</option>
                 <option value="SEMI_SWEET">SEMI SWEET</option>
                 <option value="SWEET">SWEET</option>
                 <option value="EXTRA_DRY">EXTRA DRY</option>
                 <option value="BRUT">BRUT</option>
                 <option value="EXTRA_BRUT">EXTRA BRUT</option>
                 <option value="SEC">SEC</option>
                 <option value="DEMI_SEC">DEMI SEC</option>
                 <option value="DOUX">DOUX</option>
                 </select></td></tr>

            <tr><th colspan="4"><input type="submit" value="Submit"></th></tr>
            </table>
        </form>

        <table id="wines">
            <tr>
                <th>Product no.</th>
                <th>Name</th>
                <th>Price</th>
                <th>Description</th>
                <th>Type</th>
                <th>Quantity</th>
                <th>Country</th>
                <th>Wine Region</th>
                <th>Winery</th>
                <th>Vintage</th>
                <th>Sugar Content</th>
                <th>Date of receipt</th>
            </tr>
            <tr>
                <td>${wine_created.id}</td>
                <td>${wine_created.name}</td>
                <td>${wine_created.price}</td>
                <td>${wine_created.description}</td>
                <td>${wine_created.type}</td>
                <td>${wine_created.quantity}</td>
                <td>${wine_created.country}</td>
                <td>${wine_created.wineRegion}</td>
                <td>${wine_created.winery}</td>
                <td>${wine_created.vintage}</td>
                <td>${wine_created.sugarContent}</td>
                <td>${wine_created.dateOfReceipt}</td>
                <br>
            </tr>
        </table>

<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</body>
</html>
