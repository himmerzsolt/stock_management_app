<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="iso-8859-2"%>
<html lang="en">
<head>

	<!-- Access the bootstrap Css like this,
		Spring boot will handle the resource mapping automcatically -->
	<link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />

	<!--
	<spring:url value="/css/main.css" var="springCss" />
	<link href="${springCss}" rel="stylesheet" />
	 -->
	<c:url value="/css/main.css" var="jstlCss" />
	<link href="${jstlCss}" rel="stylesheet" />

</head>
<body>


<%@include file="header.jsp" %>


<div class="container">

	<form method="POST" action="http://localhost:8080/login" class="form-signin">
		<h2 class="form-heading">Log in</h2>

		<div class="form-group ${error != null ? 'has-error' : ''}">
			<span>${message}</span>
			<input name="username" type="text" class="form-control" placeholder="Username"
				   autofocus="true"/><br>
			<input name="password" type="password" class="form-control" placeholder="Password"/>
			<span>${error}</span><br>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

			<button class="btn btn-lg btn-primary btn-block" type="submit">Log In</button><br>
			<h4 class="text-center"><a href="${contextPath}/registration">Create an account</a></h4>
		</div>

	</form>

</div>

<div class="grid-container">
	<div class="item1">
	<p>Dear User,<br />
	Welcome to your online Stock management! <br />
	You can use the following functions:</p>
	</div>
	<div class="item2">
	<ul class="functions">
	<li>List all products - click on Products in the menubar</li>
	<li>Create new product - click on New Product in the menubar</li>
	<li>Search products - click on Search in the menubar and choose from the given possibilities</li>
	<li>Change stock quantity - click on Receipt in the menubar</li>
	</ul>

	</div>
	<div class="item4">The system is still under construction, by sharing your experiences or ideas are always appreciated.</div>

<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>


</body>

</html>
