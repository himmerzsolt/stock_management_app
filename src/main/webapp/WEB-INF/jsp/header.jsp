
<nav class="navbar navbar-inverse">
    		<div class="container_newProduct">

    			<div id="navbar" class="collapse navbar-collapse">

    			 <ul class="nav navbar-nav">
    				 <li
    				 <c:if test ="${page == 'home'}">class="active" </c:if>
    				 ><a href="/home">Home</a></li>
                     <li
                     <c:if test ="${page == 'products'}">class="active" </c:if>
                     ><a href="/shop/products">Products</a></li>
                     <li
                     <c:if test ="${page == 'newproduct'}">class="active" </c:if>
                     ><a href="/shop/newProduct">New product</a></li>
                     <li
                     <c:if test ="${page == 'receipt'}">class="active" </c:if>
                     ><a href="/shop/update">Receipt</a></li>

                    <li class="dropdown">

                            <a href="#" class="dropbutton">Search</a>
                            <div class="dropdown-content">
                              <a href="http://localhost:8080/shop/searchById">Search by product number</a>
                              <a href="http://localhost:8080/shop/searchByName">Search by product name</a>
                              <a href="http://localhost:8080/shop/searchByWinery">Search by winery</a>
                              <a href="http://localhost:8080/shop/searchByWineRegion">Search by wine region</a>
                              <a href="http://localhost:8080/shop/searchByDateOfReceipt">Search by date of receipt</a>
                            </div>
                        </li>
						<li><a href="http://localhost:8080/logout">Logout</a></li>
    				</ul>
    			</div>
    		</div>
    	</nav>


