<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="iso-8859-2"%>
<html>

<head>
	<link rel="stylesheet" type="text/css" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css" />
	<c:url value="/css/main.css" var="jstlCss" />
	<link href="${jstlCss}" rel="stylesheet" />



</head>

<body>

<%@include file="header.jsp" %>


<script>
function handleClick(checkbox)
{

var tbl = document.getElementById("wines");
for(var k=0; k < document.form1.field.length; k++){
checkbox = document.form1.field[k];

    if (checkbox.checked == true) {
      for (var i = 0; i < tbl.rows.length; i++){
          tbl.rows[i].cells[document.getElementsByClassName('fields')[k].value].style.display = "table-cell";}
}

else {
for (var j = 0; j < tbl.rows.length; j++) {
           tbl.rows[j].cells[document.getElementsByClassName('fields')[k].value].style.display = "none";

}}
}
}
</script>
<form name = "form1">
<table id="checkbox">

<tr>
<td><input type="checkbox" class="fields" id="0" name="field" value="0" onclick='handleClick(this);' checked="checked"><label for="checkbox">Product no.</label></td>
<td><input type="checkbox" class="fields" id="2" name="field" value="2" onclick='handleClick(this);'checked="checked"><label for="checkbox">Price</label></td>
<td><input type="checkbox" class="fields" id="3" name="field" value="3" onclick='handleClick(this);'checked="checked"><label for="checkbox">Description</label></td>
<td><input type="checkbox" class="fields" id="4" name="field" value="4" onclick='handleClick(this);'checked="checked"><label for="checkbox">Type</label></td>
<td><input type="checkbox" class="fields" id="5" name="field" value="5" onclick='handleClick(this);'checked="checked"><label for="checkbox">Quantity</label></td>
<td><input type="checkbox" class="fields" id="6" name="field" value="6" onclick='handleClick(this);'checked="checked"><label for="checkbox">Country</label></td>
<td><input type="checkbox" class="fields" id="7" name="field" value="7" onclick='handleClick(this);'checked="checked"><label for="checkbox">Wine region</label></td>
<td><input type="checkbox" class="fields" id="8" name="field" value="8" onclick='handleClick(this);'checked="checked"><label for="checkbox">Winery</label></td>
<td><input type="checkbox" class="fields" id="9" name="field" value="9" onclick='handleClick(this);'checked="checked"><label for="checkbox">Vintage</label></td>
<td><input type="checkbox" class="fields" id="10" name="field" value="10" onclick='handleClick(this);'checked="checked"><label for="checkbox">Content of sugar</label></td>
<td><input type="checkbox" class="fields" id="11" name="field" value="11" onclick='handleClick(this);'checked="checked"><label for="checkbox">Date of receipt</label></td>
</tr>

</table>
</form>


<table id="wines">
<col class="col1"/>
<col class="col2"/>
<col class="col3"/>
<col class="col4"/>
<col class="col5"/>
<col class="col6"/>
<col class="col7"/>
<col class="col8"/>
<col class="col9"/>
<col class="col10"/>
<col class="col11"/>
<tr>
<th>Product no.</th>
<th>Name</th>
<th>Price</th>
<th>Description</th>
<th>Type</th>
<th>Quantity</th>
<th>Country</th>
<th>Wine Region</th>
<th>Winery</th>
<th>Vintage</th>
<th>Sugar Content</th>
<th>Date of receipt</th>
</tr>
<c:forEach items="${allProducts}" var="item">
<tr>
     <td>${item.id}</td>
     <td>${item.name}</td>
     <td>${item.price}</td>
     <td>${item.description}</td>
     <td>${item.type}</td>
     <td>${item.quantity}</td>
     <td>${item.country}</td>
     <td>${item.wineRegion}</td>
     <td>${item.winery}</td>
     <td>${item.vintage}</td>
     <td>${item.sugarContent}</td>
     <td>${item.dateOfReceipt}</td>
  </tr>
  </c:forEach>
</table>

	<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</body>
</html>