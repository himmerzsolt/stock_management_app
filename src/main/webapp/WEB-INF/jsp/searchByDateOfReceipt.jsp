<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="iso-8859-2"%>
<html>
<head>
    <!-- Access the bootstrap Css like this,
            Spring boot will handle the resource mapping automcatically -->
    <link rel="stylesheet" type="text/css" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css" />

    <!--
	<spring:url value="/css/main.css" var="springCss" />
	<link href="${springCss}" rel="stylesheet" />
	 -->
    <c:url value="/css/main.css" var="jstlCss" />
    <link href="${jstlCss}" rel="stylesheet" />


    <!--	<div id="demo"></div>
         	<script>
           function loadDoc() {
              var xhttp = new XMLHttpRequest();
              xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                var myObj = JSON.parse(this.responseText);
                 document.getElementById("demo").innerHTML = myObj.name;
                }
              };
              xhttp.open("GET", "http://localhost:8080/shop/webshop", true);
              xhttp.send();
            }

            document.addEventListener("DOMContentLoaded",function(){
                loadDoc();
            });
           </script>
-->

</head>
<body>

<%@include file="header.jsp" %>

<form action="http://localhost:8080/shop/findByDateOfReceipt" method="get" class="form-update">
    <table id="update">
    <tr><td>Date of receipt:</td>
    <td><input type="date" name="date" min="2019-02-01" placeholder="YYYY-MM-DD"></td></tr>
    <tr><th colspan="2"><input type="submit" value="Submit"></th></tr>
    </table>
</form>

<table id="wines">
    <tr>
        <th>Name</th>
        <th>Price</th>
        <th>Description</th>
        <th>Type</th>
        <th>Quantity</th>
        <th>Country</th>
        <th>Wine Region</th>
        <th>Winery</th>
        <th>Vintage</th>
        <th>Sugar Content</th>
        <th>Date of receipt</th>
    </tr>
    <c:forEach items="${winesByDateOfReceipt}" var="item">
    <tr>
        <td>${item.name}</td>
        <td>${item.price}</td>
        <td>${item.description}</td>
        <td>${item.type}</td>
        <td>${item.quantity}</td>
        <td>${item.country}</td>
        <td>${item.wineRegion}</td>
        <td>${item.winery}</td>
        <td>${item.vintage}</td>
        <td>${item.sugarContent}</td>
        <td>${item.dateOfReceipt}</td>
    </tr>
    </c:forEach>
</table>

<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</body>
</html>