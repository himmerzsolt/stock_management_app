<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="iso-8859-2"%>
<html>
<head>

	<!-- Access the bootstrap Css like this,
		Spring boot will handle the resource mapping automcatically -->
	<link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />

	<!--
	<spring:url value="/css/main.css" var="springCss" />
	<link href="${springCss}" rel="stylesheet" />
	 -->
	<c:url value="/css/main.css" var="jstlCss" />
	<link href="${jstlCss}" rel="stylesheet" />

</head>
<body>

	<%@include file="header.jsp" %>

	<div class="grid-container">
	<div class="item1">
	<p>Dear User,<br />
	Welcome to your online Stock management! <br />
	You can use the following functions:</p>
	</div>
	<div class="item2">
	<ul class="functions">
	<li>List all products - click on Products in the menubar</li>
	<li>Create new product - click on New Product in the menubar</li>
	<li>Search products - click on Search in the menubar and choose from the given possibilities</li>
	<li>Change stock quantity - click on Receipt in the menubar</li>
	</ul>

	</div>
	<div class="item4">The system is still under construction, by sharing your experiences or ideas are always appreciated.</div>



    </div>

	<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="${contextPath}/resources/js/bootstrap.min.js"></script>


</body>

</html>