<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="iso-8859-2"%>
<html>
<head>
    <!-- Access the bootstrap Css like this,
            Spring boot will handle the resource mapping automcatically -->
    <link rel="stylesheet" type="text/css" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css" />

    <!--
	<spring:url value="/css/main.css" var="springCss" />
	<link href="${springCss}" rel="stylesheet" />
	 -->
    <c:url value="/css/main.css" var="jstlCss" />
    <link href="${jstlCss}" rel="stylesheet" />


    <!--	<div id="demo"></div>
         	<script>
           function loadDoc() {
              var xhttp = new XMLHttpRequest();
              xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                var myObj = JSON.parse(this.responseText);
                 document.getElementById("demo").innerHTML = myObj.name;
                }
              };
              xhttp.open("GET", "http://localhost:8080/shop/webshop", true);
              xhttp.send();
            }

            document.addEventListener("DOMContentLoaded",function(){
                loadDoc();
            });
           </script>
-->

</head>
<body>

<%@include file="header.jsp" %>

<form action="http://localhost:8080/shop/findById" method="get" class="form-update">
    <table id="update">
    <tr><td>Product ID:</td>
    <td><input type="text" name="id"></td></tr>
    <tr><th colspan="2"><input type="submit" value="Submit"></th></tr>
    </table>
</form>



<table id="wines">
    <tr>
        <th>Product no.</th>
        <th>Name</th>
        <th>Price</th>
        <th>Description</th>
        <th>Type</th>
        <th>Quantity</th>
        <th>Country</th>
        <th>Wine Region</th>
        <th>Winery</th>
        <th>Vintage</th>
        <th>Sugar Content</th>
        <th>Date of receipt</th>
    </tr>
    <tr>
        <td>${wine.id}</td>
        <td>${wine.name}</td>
        <td>${wine.price}</td>
        <td>${wine.description}</td>
        <td>${wine.type}</td>
        <td>${wine.quantity}</td>
        <td>${wine.country}</td>
        <td>${wine.wineRegion}</td>
        <td>${wine.winery}</td>
        <td>${wine.vintage}</td>
        <td>${wine.sugarContent}</td>
        <td>${wine.dateOfReceipt}</td>
    </tr>

</table>

<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</body>
</html>