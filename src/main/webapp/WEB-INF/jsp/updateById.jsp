<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="iso-8859-2"%>
<html>
<head>
    <!-- Access the bootstrap Css like this,
            Spring boot will handle the resource mapping automcatically -->
    <link rel="stylesheet" type="text/css" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css" />

    <c:url value="/css/main.css" var="jstlCss" />
    <link href="${jstlCss}" rel="stylesheet" />

</head>
<body>

<%@include file="header.jsp" %>

<form action="http://localhost:8080/shop/updateById" method="post" class="form-update">

   <input type="hidden"
            feature-roles2
           name="${_csrf.parameterName}"
           value="${_csrf.token}"/>

<table id="update">
    <tr><td><label>Product ID:*</label></td>
    <td><input type="text" name="id"></td></tr>
    <tr><td><label>Quantity:*</label></td>
    <td><input type="number" name="quantity"></td></tr>
    <tr><th colspan="2"><input type="submit" value="Submit"></th></tr>
    </table>
</form>

<table id="wines">
            <tr>
                <th>Product no.</th>
                <th>Name</th>
                <th>Price</th>
                <th>Description</th>
                <th>Type</th>
                <th>Quantity</th>
                <th>Country</th>
                <th>Wine Region</th>
                <th>Winery</th>
                <th>Vintage</th>
                <th>Sugar Content</th>
                <th>Date of receipt</th>
            </tr>
            <tr>
                <td>${wine_updated.id}</td>
                <td>${wine_updated.name}</td>
                <td>${wine_updated.price}</td>
                <td>${wine_updated.description}</td>
                <td>${wine_updated.type}</td>
                <td>${wine_updated.quantity}</td>
                <td>${wine_updated.country}</td>
                <td>${wine_updated.wineRegion}</td>
                <td>${wine_updated.winery}</td>
                <td>${wine_updated.vintage}</td>
                <td>${wine_updated.sugarContent}</td>
                <td>${wine_updated.dateOfReceipt}</td>
                <br>
            </tr>
        </table>

<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</body>
</html>