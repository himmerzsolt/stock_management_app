<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="iso-8859-2"%>
<html>
<head>
    <!-- Access the bootstrap Css like this,
            Spring boot will handle the resource mapping automcatically -->
    <link rel="stylesheet" type="text/css" href="/webjars/bootstrap/3.3.7/css/bootstrap.min.css" />

    <c:url value="/css/main.css" var="jstlCss" />
    <link href="${jstlCss}" rel="stylesheet" />

</head>
<body>

<%@include file="header.jsp" %>

<form action="http://localhost:8080/shop/findByWinery" method="get" class="form-update">
    <table id="update">
    <tr><td>Name of the winery:</td>
    <td><input type="text" name="winery"></td></tr>
    <tr><th colspan="2"><input type="submit" value="Submit"></th></tr>
    </table>
</form>

<table id="wines">
    <tr>
        <th>Product no.</th>
        <th>Name</th>
        <th>Price</th>
        <th>Description</th>
        <th>Type</th>
        <th>Quantity</th>
        <th>Country</th>
        <th>Wine Region</th>
        <th>Winery</th>
        <th>Vintage</th>
        <th>Sugar Content</th>
        <th>Date of receipt</th>
    </tr>

    <c:forEach items="${wines_winery}" var="item">
    <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.price}</td>
        <td>${item.description}</td>
        <td>${item.type}</td>
        <td>${item.quantity}</td>
        <td>${item.country}</td>
        <td>${item.wineRegion}</td>
        <td>${item.winery}</td>
        <td>${item.vintage}</td>
        <td>${item.sugarContent}</td>
        <td>${item.dateOfReceipt}</td>
    </tr>
    </c:forEach>

     <c:if test="${empty wines_winery}">
            <tr>
            <td>No such wine in database</td>
            </tr>
            </c:if>
</table>

<script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>

</body>
</html>