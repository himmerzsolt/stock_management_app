package com.matritel.stockmanagement.controller;

import com.matritel.stockmanagement.model.ProductType;
import com.matritel.stockmanagement.model.StockItemDto;
import com.matritel.stockmanagement.model.SugarLevel;
import com.matritel.stockmanagement.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Optional;

@Controller
public class ProductController {

    @Autowired
    StockService stockService;

    @RequestMapping("/shop/products")
    public String welcome(Map<String, Object> model) {
        model.put("allProducts", stockService.findAll());
        model.put("page", "products");
        return "products";
    }

    @RequestMapping(value = "/shop/searchById")
    public String findById() {
        return "searchbyid";
    }

    @RequestMapping(value = "/shop/findById")
    public String getById(@RequestParam(name = "id") long id, Map<String, Object> model) {
        if (stockService.findById(id).isPresent()) {
            model.put("wine", stockService.findById(id).get());
            return "searchbyid";
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product does not exist by the given parameter(s)!");
    }

    @RequestMapping(value = "/shop/searchByName")
    public String getByName() {
        return "searchbyname";
    }

    @RequestMapping(value = "/shop/findByName")
    public String findByName(@RequestParam(name = "name") String name, Map<String, Object> model) {
        model.put("wines_name", stockService.findByName(name));
        return "searchbyname";
    }

    @RequestMapping(value = "/shop/newProduct")
    public String createNewProduct(Map<String, Object> model) {
        model.put("message", "");
        model.put("page", "newproduct");
        return "newProduct";
    }

    @RequestMapping(value = "/shop/addNewItem")
    public String createNewWebShopItem(@RequestParam(name = "name") String name, @RequestParam(name = "price") int price,
                                       @RequestParam(name = "description") String description, @RequestParam(name = "type") ProductType type,
                                       @RequestParam(name = "quantity") int quantity, @RequestParam(name = "country") String country,
                                       @RequestParam(name = "wineRegion") String wineRegion, @RequestParam(name = "winery") String winery,
                                       @RequestParam(name = "vintage") String vintage, @RequestParam(name = "sugarContent") SugarLevel sugarContent,
                                       HttpServletResponse responseStatus, Map<String, Object> model) {
        if (stockService.isItemExistInDb(name, winery, vintage)) {
            responseStatus.setStatus(200);
        } else {
            responseStatus.setStatus(201);
        }
        model.put("wine_created", stockService.update(name, price, description, type, quantity, country, wineRegion, winery, vintage, sugarContent));
        return "newProduct";
    }

    @RequestMapping(value = "/shop/searchByWinery")
    public String getByWinery() {
        return "searchByWinery";
    }

    @RequestMapping(value = "/shop/findByWinery")
    public String findByWinery(@RequestParam(name = "winery") String winery, Map<String, Object> model) {
        model.put("wines_winery", stockService.findByWinery(winery));
        return "searchByWinery";
    }

    @RequestMapping(value = "/shop/searchByWineRegion")
    public String searchByWineRegion() {
        return "searchByWineRegion";
    }

    @RequestMapping(value = "/shop/findByWineRegion")
    public String findByWineRegion(@RequestParam(name = "wineRegion") String wineRegion, Map<String, Object> model) {
        model.put("winesByRegion", stockService.findByWineRegion(wineRegion));
        return "searchByWineRegion";
    }

    @RequestMapping(value = "/shop/searchByDateOfReceipt")
    public String searchByDateOfReceipt() {
        return "searchByDateOfReceipt";
    }

    @RequestMapping(value = "/shop/findByDateOfReceipt")
    public String findByDateOfReceipt(@RequestParam(name = "date") String date, Map<String, Object> model) {
        model.put("winesByDateOfReceipt", stockService.findByDateOfReceipt(date));
        return "searchByDateOfReceipt";
    }

    @RequestMapping(value = "/shop/update")
    public String update(Map<String, Object> model) {
        model.put("page", "receipt");
        return "updateById";
    }

    @RequestMapping(value = "shop/updateById")
    public String updateById(@RequestParam long id, @RequestParam int quantity, Map<String, Object> model) {
        Optional<StockItemDto> dto = stockService.findById(id);
        if (dto.isPresent()) {
            model.put("wine_updated", stockService.updateById(id, quantity).get());
            return "updateById";
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product does not exist by the given parameter(s)!");
    }
}
