package com.matritel.stockmanagement.model;

import lombok.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StockItemDto {
    private long id;

    @Size(max = 40)
    @NotNull
    private String name;

    @Size(max = 12)
    @NotNull
    private int price;

    @Size(max = 255)
    private String description;

    @NotNull
    private ProductType type;

    @Size(max = 12)
    @NotNull
    private int quantity;

    @Size(max = 40)
    @NotNull
    private String country;

    @Size(max = 40)
    private String wineRegion;

    @Size(max = 40)
    @NotNull
    private String winery;

    @Size(max = 12)
    @NotNull
    private String vintage;

    @NotNull
    private SugarLevel sugarContent;

    private LocalDate dateOfReceipt;
}
