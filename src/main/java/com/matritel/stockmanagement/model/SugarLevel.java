package com.matritel.stockmanagement.model;

public enum SugarLevel {
    DRY, SEMI_DRY, SEMI_SWEET, SWEET, EXTRA_DRY, BRUT, EXTRA_BRUT, SEC, DEMI_SEC, DOUX
}
