package com.matritel.stockmanagement.model;

public enum ProductType {
    WHITE, RED, CHAMPAGNE, COGNAC, FRIZZANTE, SPARKLING
}
