package com.matritel.stockmanagement.repository.entity;

import com.matritel.stockmanagement.model.ProductType;
import com.matritel.stockmanagement.model.SugarLevel;
import lombok.*;
import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "stock")
@SequenceGenerator(name = "seq", initialValue = 1000, allocationSize = 1)
public class StockItemEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    private long id;

    @Column(columnDefinition = "VARCHAR(40)")
    private String name;

    @Column(columnDefinition = "INT(12)")
    private int price;

    @Column(columnDefinition = "VARCHAR(255)")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(length = 9)
    private ProductType type;

    @Column(columnDefinition = "INT(12)")
    private int quantity;

    @Column(columnDefinition = "VARCHAR(40)")
    private String country;

    @Column(name = "wine_region", columnDefinition = "VARCHAR(40)")
    private String wineRegion;

    @Column(columnDefinition = "VARCHAR(40)")
    private String winery;

    @Column(columnDefinition = "VARCHAR(12)")
    private String vintage;

    @Enumerated(EnumType.STRING)
    @Column(length = 10)
    private SugarLevel sugarContent;

    @Column(name = "date_of_receipt")
    private LocalDate dateOfReceipt;
}


