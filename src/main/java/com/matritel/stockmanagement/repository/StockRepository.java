package com.matritel.stockmanagement.repository;

import com.matritel.stockmanagement.repository.entity.StockItemEntity;
import org.springframework.data.repository.CrudRepository;
import java.time.LocalDate;
import java.util.List;

public interface StockRepository extends CrudRepository<StockItemEntity, Long> {
    List<StockItemEntity> findAll();
    List<StockItemEntity> findByDateOfReceipt(LocalDate date);
}
