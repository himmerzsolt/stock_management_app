package com.matritel.stockmanagement.service;

import com.matritel.stockmanagement.model.ProductType;
import com.matritel.stockmanagement.model.StockItemDto;
import com.matritel.stockmanagement.model.SugarLevel;
import com.matritel.stockmanagement.repository.StockRepository;
import com.matritel.stockmanagement.repository.entity.StockItemEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StockService {

    @Autowired
    private StockRepository repository;

    private StockItemEntity toEntity(StockItemDto dto) {
        return StockItemEntity.builder()
                .name(dto.getName())
                .price(dto.getPrice())
                .description(dto.getDescription())
                .type(dto.getType())
                .quantity(dto.getQuantity())
                .country(dto.getCountry())
                .wineRegion(dto.getWineRegion())
                .winery(dto.getWinery())
                .vintage(dto.getVintage())
                .sugarContent(dto.getSugarContent())
                .dateOfReceipt(LocalDate.now())
                .build();
    }

    private StockItemDto toDto(StockItemEntity entity) {
        return StockItemDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .price(entity.getPrice())
                .description(entity.getDescription())
                .type(entity.getType())
                .quantity(entity.getQuantity())
                .country(entity.getCountry())
                .wineRegion(entity.getWineRegion())
                .winery(entity.getWinery())
                .vintage(entity.getVintage())
                .sugarContent(entity.getSugarContent())
                .dateOfReceipt(entity.getDateOfReceipt())
                .build();
    }

    private StockItemDto createItem(StockItemDto inputDto) {
        StockItemEntity toSave = toEntity(inputDto);
        StockItemEntity saved = repository.save(toSave);
        return toDto(saved);
    }

    public boolean isItemExistInDb(String name, String winery, String vintage) {
        List<StockItemEntity> entityList = repository.findAll();
        if (!entityList.isEmpty()) {
            for (StockItemEntity entity : entityList) {
                if (entity.getName().toLowerCase().equals(name.toLowerCase())
                        && entity.getWinery().toLowerCase().equals(winery.toLowerCase())
                        && entity.getVintage().toLowerCase().equals(vintage.toLowerCase())) {
                    return true;
                }
            }
        }
        return false;
    }

    public StockItemDto update(String name, int price, String description, ProductType type, int quantity,
                               String country, String wineRegion, String winery, String vintage,
                               SugarLevel sugarContent) {
        List<StockItemEntity> entityList = repository.findAll();
        for (StockItemEntity entity : entityList) {
            if (entity.getName().toLowerCase().equals(name.toLowerCase())
                    && entity.getWinery().toLowerCase().equals(winery.toLowerCase())
                    && entity.getVintage().toLowerCase().equals(vintage.toLowerCase())) {
                entity.setQuantity(entity.getQuantity() + quantity);
                entity.setDateOfReceipt(LocalDate.now());
                repository.save(entity);
                return (toDto(entity));
            }
        }
        StockItemDto inputDto = new StockItemDto();
        inputDto.setName(name);
        inputDto.setPrice(price);
        inputDto.setDescription(description);
        inputDto.setType(type);
        inputDto.setQuantity(quantity);
        inputDto.setCountry(country);
        inputDto.setWineRegion(wineRegion);
        inputDto.setWinery(winery);
        inputDto.setVintage(vintage);
        inputDto.setSugarContent(sugarContent);

        return (createItem(inputDto));
    }

    public Optional<StockItemDto> updateById(long id, int quantity) {
        Optional<StockItemEntity> optEntity = repository.findById(id);
        if (optEntity.isPresent()) {
            StockItemEntity entity = optEntity.get();
            entity.setQuantity(entity.getQuantity() + quantity);
            repository.save(entity);
            return Optional.of(toDto(optEntity.get()));
        }
        return Optional.empty();
    }

    public List<StockItemDto> findAll() {
        List<StockItemDto> dtoList = new ArrayList<>();
        List<StockItemEntity> entityList = repository.findAll();

        for (StockItemEntity entity : entityList) {
            dtoList.add(toDto(entity));
        }
        return dtoList;
    }

    public Optional<StockItemDto> findById(long id) {
        Optional<StockItemEntity> optEntity = repository.findById(id);
        return optEntity.map(this::toDto);
    }

    public void deleteById(long id) {
        repository.deleteById(id);
    }

    public List<StockItemDto> findByName(String name) {
        List<StockItemEntity> entityList = repository.findAll();
        List<StockItemDto> dtoList = new ArrayList<>();

        for (StockItemEntity entity : entityList) {
            if (entity.getName().toLowerCase().contains(name.toLowerCase())) {
                dtoList.add(toDto(entity));
            }
        }
        return dtoList;
    }

    public List<StockItemDto> findByWinery(String winery) {
        List<StockItemEntity> entityList = repository.findAll();
        List<StockItemDto> dtoList = new ArrayList<>();
        for (StockItemEntity entity : entityList) {
            if (entity.getWinery().toLowerCase().contains(winery.toLowerCase())) {
                dtoList.add(toDto(entity));
            }
        }
        return dtoList;
    }

    public List<StockItemDto> findByWineRegion(String region) {
        List<StockItemEntity> entityList = repository.findAll();
        List<StockItemDto> dtoList = new ArrayList<>();
        for (StockItemEntity entity : entityList) {
            if (entity.getWineRegion().toLowerCase().contains(region.toLowerCase()))
                dtoList.add(toDto(entity));
        }
        return dtoList;
    }

    public List<StockItemDto> findByDateOfReceipt(String date) {
        //convert String to LocalDate
        LocalDate localDate = LocalDate.parse(date);
        List<StockItemEntity> entityList = repository.findByDateOfReceipt(localDate);
        List<StockItemDto> dtoList = new ArrayList<>();
        for (StockItemEntity entity : entityList) {
            dtoList.add(toDto(entity));
        }
        return dtoList;
    }
}


