package com.matritel.stockmanagement.service;

import com.matritel.stockmanagement.model.User;

public interface UserService {
    void save(User user);
    User findByUsername(String username);
}


